const ButtonAddList = document.getElementById('addList')
const ButtonAddCard = document.getElementById('addCard')

ButtonAddList.addEventListener('click', CreatNewFormList)
ButtonAddCard.addEventListener('click', CreatNewFormCard)

const AllList = []

function CreatList() {
    const Inputs = document.querySelectorAll('input')
    const newList = new List(Inputs[0].value, Inputs[1].value)
    AllList.push(newList)
    const Form = document.getElementById('Form')
    const Lists = document.getElementById('Lists')

    document.body.removeChild(Form)
    Lists.innerHTML = ''

    AllList.forEach(element => {
        const DivList = document.createElement('div')
        DivList.id = `${element.Name}`
        DivList.class = 'List'
        const title = document.createElement('h1')
        title.textContent = `${element.Name}`
        const category = document.createElement('h3')
        category.textContent = `${element.Category}`
        const Lists = document.getElementById('Lists')

        DivList.appendChild(title)
        DivList.appendChild(category)
        Lists.appendChild(DivList)
    })
}

ButtonSendList.addEventListener('click', CreatList)
ButtonSendCard.addEventListener('click', CreatCard)



function CreatCard() {
    let arrCard = []
    const Inputs = document.querySelector('input')
    arrCard.push(Inputs)
    const Tasks = document.querySelector('textarea')
    arrCard.push(Tasks)
    const newCard = new Card(arrCard[0].value, arrCard[1].value)
    AllList.forEach(element => {
        if (element.Name === newCard.Name) {
            element.Cards.push(newCard)
        } else {
            alert('não há lista com esse nome')
        }

        element.Cards.forEach(element => {
            const Form = document.getElementById('Form')
            document.body.removeChild(Form)
            const DivList = document.getElementById(`${element.Name}`)

            const DivCard = document.createElement('div')
            DivCard.id = `Card${element.Name}`
            DivCard.classList.add('Card')
            DivCard.textContent = `${element.Tasks}`

            DivList.appendChild(DivCard)
        })
    })
}