class List {
    constructor(ListName, Category, Cards){
        this.Name = ListName,
        this.Category = Category
        this.Cards = []
    }
}

class Card {
    constructor(CardName, CardTasks){
        this.Name = CardName,
        this.Tasks = CardTasks
    }
}

let ButtonSendList = document.createElement('button')
const CreatNewFormList = () => {
    const Form = document.createElement('div')
    Form.id = 'Form'
    const InputListName = document.createElement('input')
    InputListName.placeholder = 'Digite o nome da sua lista'
    const InputListCategory = document.createElement('input')
    InputListCategory.placeholder = 'Digite a categoria de sua lista'
    ButtonSendList.id = 'ButtonSend'
    ButtonSendList.textContent = 'Enviar'

    Form.appendChild(InputListName)
    Form.appendChild(InputListCategory)
    Form.appendChild(ButtonSendList)
    document.body.appendChild(Form)
    ButtonSendList = document.getElementById('ButtonSend')
}

let ButtonSendCard = document.createElement('button')
const CreatNewFormCard = () => {
    const Form = document.createElement('div')
    Form.id = 'Form'
    const InputCardName = document.createElement('input')
    InputCardName.placeholder = 'Digite o nome do seu Card'
    const InputCardTask = document.createElement('textarea')
    InputCardTask.placeholder = 'Digite aqui suas tarefas'
    ButtonSendCard.id = 'ButtonSend'
    ButtonSendCard.textContent = 'Enviar'

    Form.appendChild(InputCardName)
    Form.appendChild(InputCardTask)
    Form.appendChild(ButtonSendCard)
    document.body.appendChild(Form)
    ButtonSendCard = document.getElementById('ButtonSend')
}
